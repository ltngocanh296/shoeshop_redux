import React, { Component } from 'react'
import ItemShoe from './ItemShoe';
import {connect} from 'react-redux';

class ListShoe extends Component {
  renderListShoe = () => {
     return this.props.list.map((itemShoe) => { return <ItemShoe 
      data={itemShoe} 
      /> });

  };
  
render() {
  return (
    <div className='row'>{this.renderListShoe()}</div>
  )
}
}
let mapStateToProps= (state) => { 
return {
  list: state.shoeReducer.shoeArr,
}
}
export default connect(mapStateToProps)(ListShoe);
