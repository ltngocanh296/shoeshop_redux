import { ADD_TO_CART, DETAIL_SHOE } from "../constant/shoeConstant";
import { dataShoe } from "../dataShoe";

let initialState = {
    shoeArr:dataShoe,
    detail:dataShoe[0],
    cart: [],

};
export const shoeReducer = (state=initialState, action) => {
    switch(action.type) {
        case ADD_TO_CART: {
            let updateCart = [...state.cart];
            let index = updateCart.findIndex((item) => { 
                return item.id == action.payload.id;
             });
             if (index == -1) {
                let cartItem = {...action.payload,number:1};
                updateCart.pust(cartItem);
             }else {
                updateCart[index].number++;
             }
             return {...state, cart:updateCart};
        }
        case DETAIL_SHOE: {
            state.detail=action.payload;
            return {...state};
        }
        default:
            return state;

    }

}